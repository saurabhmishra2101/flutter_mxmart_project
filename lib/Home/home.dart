import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_app_mxmart/HomeTab/hometab.dart';
import 'package:flutter_app_mxmart/LiveSalesTab/livesalestab.dart';
import 'package:flutter_app_mxmart/ProfileTab/profiletab.dart';
import 'package:flutter_app_mxmart/TransactionTab/transactiontab.dart';
import 'package:flutter_app_mxmart/Utility/utility.dart';
import 'dart:async';
import 'dart:convert';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:percent_indicator/percent_indicator.dart';

class HomePageApp extends StatefulWidget {
  @override

  HomePageRoute createState()=>HomePageRoute();
}

class HomePageRoute extends State<HomePageApp> {

  int _selectedTabIndex = 0;

  List _pages = [
    //  Text("Home"),
    HomePage(),
    LiveSalesPage(),
    TransactionsPage(),
    ProfilePage(),
  ];

  _changeIndex(int index) {
    setState(() {
      _selectedTabIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text("Mxmart"),
          automaticallyImplyLeading: false,
          elevation: 0.0,
          backgroundColor: new Color(Utility.hexToInt("FFF15922"))),
      body: Center(child: _pages[_selectedTabIndex]),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedTabIndex,
        onTap: _changeIndex,
        selectedItemColor: new Color(Utility.hexToInt("FFF15922")),
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(icon : ImageIcon(AssetImage("assets/images/home.png")),backgroundColor: new Color(Utility.hexToInt("FFF15922")), title: Text("Home")),
          BottomNavigationBarItem(
              icon:ImageIcon(AssetImage("assets/images/trade.png")),backgroundColor: new Color(Utility.hexToInt("FFF15922")), title: Text("Live Sales")),
          BottomNavigationBarItem(
              icon:ImageIcon(AssetImage("assets/images/transaction.png")),backgroundColor: new Color(Utility.hexToInt("FFF15922")), title: Text("Transactions")),
          BottomNavigationBarItem(
              icon:ImageIcon(AssetImage("assets/images/user.png")), backgroundColor: new Color(Utility.hexToInt("FFF15922")),title: Text("Profile")),
        ],
      ),
    );
  }

}

