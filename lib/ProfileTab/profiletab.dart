import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_app_mxmart/Utility/utility.dart';
import 'dart:async';
import 'dart:convert';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:percent_indicator/percent_indicator.dart';

class ProfilePage extends StatelessWidget {
  static Route<dynamic> route() => MaterialPageRoute(
    builder: (context) => ProfilePage(),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(

        body: (
            Padding(
              padding: EdgeInsets.all(0),

              child: ListView(
                  children: <Widget>[
                    Stack(
                        alignment: Alignment.topCenter,


                        children: [
                          Container(

                            child: Align(
                                child: Text("User Code:1023",style: TextStyle(color: Color.fromARGB(255, 255, 255, 255)),),alignment: Alignment.bottomRight),
                            decoration: new BoxDecoration(color: new Color(Utility.hexToInt("FFF15922"))),
                            height: 80,
                            padding: EdgeInsets.all(10),




                          ),


                          FractionalTranslation(
                            translation: Offset(0.0, 0.4),
                            child: Align(
                              child: CircleAvatar(
                                backgroundColor:new Color(Utility.hexToInt("0000FFFF")) ,
                                child: ClipOval(

                                  child: Image.network(
                                      "https://i.imgur.com/BoN9kdC.png",
                                      width: 100,
                                      fit: BoxFit.fill


                                  ),

                                ),
                                radius: 50,
                              ),
                              alignment: FractionalOffset(0.5, 0.0),
                            ),
                          ),

                        ]),

                    Container(
                        alignment: Alignment.topCenter,
                        margin: EdgeInsets.fromLTRB(0, 30, 0, 0),
                        padding: EdgeInsets.only(top: 25.0,bottom: 5.0),
                        child: Text(
                          'Saurabh Technology Solutions',
                          style: TextStyle(fontSize: 17),
                        )),

                    Container(
                      //  alignment: Alignment.center,

                        child: Text(
                          'Bhilai',
                          style: TextStyle(fontSize: 15,),

                          textAlign: TextAlign.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 0, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Complete your profile',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(50, 0, 100, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(0, 0, 30, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '70%',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    new LinearPercentIndicator(
                      width: 350,
                      lineHeight: 4.0,
                      percent: 0.7,
                      padding: EdgeInsets.all(10),
                      alignment: MainAxisAlignment.center,
                      backgroundColor: Colors.grey,
                      progressColor: new Color(Utility.hexToInt("FFF15922")),
                    ),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'PERSONAL DETAILS',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(50, 20, 100, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(0, 20, 30, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'EDIT',
                                  style: TextStyle(fontSize: 15,color: new Color(Utility.hexToInt("FFF15922"))),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Name',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(50, 20, 70, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Saurabh Technology',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 5, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'DOB',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(50, 5, 80, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '21/01/1987',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 5, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Company Type',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(50, 5, 10, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Partnership',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 5, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Company Name',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(0, 3, 3, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                width: 200,
                                margin: EdgeInsets.fromLTRB(0, 5, 10, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Skillmine Technology Consulting Pvt Ltd',
                                  style: TextStyle(fontSize: 15,),
                                  overflow: TextOverflow.ellipsis,


                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 5, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Designation',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(0, 3, 80, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,

                                margin: EdgeInsets.fromLTRB(0, 5, 20, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Consultant',
                                  style: TextStyle(fontSize: 15,),



                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 5, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Email',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(70, 3, 35, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                width: 170,
                                margin: EdgeInsets.fromLTRB(0, 5, 35, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'saurabh.mishra@skill-mine.com',
                                  style: TextStyle(fontSize: 15,),

                                  overflow: TextOverflow.ellipsis,

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 5, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Landline',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(70, 3, 33, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,

                                margin: EdgeInsets.fromLTRB(0, 5, 33, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '08066666969',
                                  style: TextStyle(fontSize: 15,),

                                  overflow: TextOverflow.ellipsis,

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 5, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Mobile',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(80, 3, 33, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,

                                margin: EdgeInsets.fromLTRB(0, 5, 33, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '8050952462',
                                  style: TextStyle(fontSize: 15,),

                                  overflow: TextOverflow.ellipsis,

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 5, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Address',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(13, 3, 0, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,

                              margin: EdgeInsets.fromLTRB(43, 5, 0, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                              child: Text(
                                'HAL 3rd Stage MURUGESHPALYA',
                                style: TextStyle(fontSize: 15,),

                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,

                                textAlign: TextAlign.right,
                              ), width:155,),


                          ],

                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),
                    const Divider(
                      color: Colors.black,
                      height: 20,
                      thickness: 0.1,
                      indent: 20,
                      endIndent: 0,
                    ),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'BUSINESS DETAILS',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(50, 20, 100, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(0, 20, 30, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'EDIT',
                                  style: TextStyle(fontSize: 15,color: new Color(Utility.hexToInt("FFF15922"))),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'PAN No.',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(50, 20, 55, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'AXDDF3423G',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),


                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'CIN No.',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(50, 10, 57, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'RJDFKG',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'GSTIN',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(58, 10, 57, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '29KAUKDSFD',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'UAN',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(70, 10, 57, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '232345453534',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Billing Address:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(0, 10, 37, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                width: 150.0,
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Green Glen Layout Belandur Bangalore',
                                  style: TextStyle(fontSize: 15,),
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'City:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(70, 10, 58, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Bangalore',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'State:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(62, 10, 58, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Karnataka',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Pin Code:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(36, 10, 58, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '560098',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Pin Code:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(36, 10, 58, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '560098',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Establishment Date:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 10, 0, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '23/03/2020',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'BANK DETAILS',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(50, 20, 100, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(25, 20, 30, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'EDIT',
                                  style: TextStyle(fontSize: 15,color: new Color(Utility.hexToInt("FFF15922"))),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Bank Name:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 10, 0, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(55, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'BOI',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Branch:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 10, 30,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(55, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Indira Nagar',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Account No:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 10, 30,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(21, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '45768789789',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'IFSC Code:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 10, 30,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'BOI789789',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'FOR OFFICE USE ONLY',
                                  style: TextStyle(fontSize: 13,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(50, 20, 70, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            /*Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(25, 20, 30, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'EDIT',
                                  style: TextStyle(fontSize: 13,color: new Color(Utility.hexToInt("FFF15922"))),

                                  textAlign: TextAlign.right,
                                )),

*/

                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Group Name:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 10, 20,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '--',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Group Code:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 10, 25,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '0',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Location:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 10, 45,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '--',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    const Divider(
                      color: Colors.black,
                      height: 20,
                      thickness: 0.1,
                      indent: 20,
                      endIndent: 0,
                    ),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'CONTACT DETAILS',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(50, 20, 70, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(25, 20, 30, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'EDIT',
                                  style: TextStyle(fontSize: 15,color: new Color(Utility.hexToInt("FFF15922"))),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'ACCOUNTS',
                                  style: TextStyle(fontSize: 13,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(50, 20, 70, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            /*Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(25, 20, 30, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'EDIT',
                                  style: TextStyle(fontSize: 13,color: new Color(Utility.hexToInt("FFF15922"))),

                                  textAlign: TextAlign.right,
                                )),

*/

                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Name:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 10, 60,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Saurabh',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Phone Number:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 10, 0,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '08022332343',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Mobile Number:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 10, 0,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(25, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '8722332343',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Email ID:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 10, 50,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(25, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'saurabh@gmail.com',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'SALES/MARKETING',
                                  style: TextStyle(fontSize: 13,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(50, 20, 70, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            /*Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(25, 20, 30, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'EDIT',
                                  style: TextStyle(fontSize: 13,color: new Color(Utility.hexToInt("FFF15922"))),

                                  textAlign: TextAlign.right,
                                )),

*/

                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Name:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 10, 60,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Vinod',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Phone Number:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 10, 0,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '08067574678',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Mobile Number:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 10, 0,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(25, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '8587854468',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Email ID:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 10, 50,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(25, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'vinod@gmail.com',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'DISPATCH',
                                  style: TextStyle(fontSize: 13,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(50, 20, 70, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            /*Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(25, 20, 30, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'EDIT',
                                  style: TextStyle(fontSize: 13,color: new Color(Utility.hexToInt("FFF15922"))),

                                  textAlign: TextAlign.right,
                                )),

*/

                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Name:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 10, 60,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Rahul',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Phone Number:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 10, 0,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '08094567578',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Mobile Number:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 10, 0,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(25, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '9966758789',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Email ID:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 10, 50,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(25, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'rahul@gmail.com',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    const Divider(
                      color: Colors.black,
                      height: 20,
                      thickness: 0.1,
                      indent: 20,
                      endIndent: 0,
                    ),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'INSTALLED CAPACITY',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(50, 20, 70, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(25, 20, 30, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'ADD',
                                  style: TextStyle(fontSize: 15,color: new Color(Utility.hexToInt("FFF15922"))),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),


                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'TYPE OF MATERIALS',
                                  style: TextStyle(fontSize: 13,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(25, 20, 10, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(0, 20, 30, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'CAPACITY',
                                  style: TextStyle(fontSize: 13,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Pellets',
                                  style: TextStyle(fontSize: 13,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(45, 20, 13, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(60, 20, 30, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '10000 MT/day',
                                  style: TextStyle(fontSize: 13,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),


                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Sponge Iron',
                                  style: TextStyle(fontSize: 13,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(45, 10, 13, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 30, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '78 MT/day',
                                  style: TextStyle(fontSize: 13,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Ingots',
                                  style: TextStyle(fontSize: 13,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(60, 10, 13, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(50, 10, 30, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '0 MT/day',
                                  style: TextStyle(fontSize: 13,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Billets',
                                  style: TextStyle(fontSize: 13,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(60, 10, 13, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(50, 10, 30, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '0 MT/day',
                                  style: TextStyle(fontSize: 13,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    const Divider(
                      color: Colors.black,
                      height: 20,
                      thickness: 0.1,
                      indent: 20,
                      endIndent: 0,
                    ),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'REFERENCE',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(50, 20, 70, 0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(90, 20, 10, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'ADD',
                                  style: TextStyle(fontSize: 15,color: new Color(Utility.hexToInt("FFF15922"))),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Company Name:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(23, 10, 0,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(25, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Skillmine',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 0, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Name:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(23, 10, 0,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(92, 0, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Saurabh',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 0, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Mobile Number:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(23, 10, 0,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(28, 0, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '8050952462',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 0, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Email ID:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(23, 10, 0,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(77, 0, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'saurabh@gmail.com',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    const Divider(
                      color: Colors.black,
                      height: 20,
                      thickness: 0.1,
                      indent: 20,
                      endIndent: 0,
                    ),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Company Name:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(23, 10, 0,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(25, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Steel36',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 0, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Name:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(23, 10, 0,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(92, 0, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Vinod',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 0, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Mobile Number:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(23, 10, 0,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(28, 0, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '9076808765',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 0, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Email ID:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(23, 10, 0,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(77, 0, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'vinod@gmail.com',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    const Divider(
                      color: Colors.black,
                      height: 20,
                      thickness: 0.1,
                      indent: 20,
                      endIndent: 0,
                    ),
                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Company Name:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(23, 10, 0,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(25, 10, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Mxmart',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 0, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Name:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(23, 10, 0,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(92, 0, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Rahul',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 0, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Mobile Number:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(23, 10, 0,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(28, 0, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  '9767675456',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    Container(
                        child: Row(
                          children: <Widget>[



                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(31, 0, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'Email ID:',
                                  style: TextStyle(fontSize: 15,),


                                  textAlign: TextAlign.left,
                                )),



                            Container(
                              //  alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(23, 10, 0,0),
                              padding: EdgeInsets.only(top: 5.0,bottom: 2.0),

                            ),


                            Container(
                              //  alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(77, 0, 0, 0),
                                padding: EdgeInsets.only(top: 5.0,bottom: 2.0),
                                child: Text(
                                  'rahul@gmail.com',
                                  style: TextStyle(fontSize: 15,),

                                  textAlign: TextAlign.right,
                                )),



                          ],
                          // mainAxisAlignment: MainAxisAlignment.center,
                        )),

                    const Divider(
                      color: Colors.black,
                      height: 20,
                      thickness: 0.1,
                      indent: 20,
                      endIndent: 0,
                    ),

                  ]),


            )));
  }
}
