import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_app_mxmart/Forget_Password/forgetpassword.dart';
import 'package:flutter_app_mxmart/Login/login.dart';
import 'package:flutter_app_mxmart/Utility/utility.dart';
import 'dart:async';
import 'dart:convert';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class OtpRegistrationApp extends StatefulWidget {
  final int otp;
  final int emailotp;
  final String email;
  final String mobile;

  // MyApp2(String otp);
  //MyApp2({Key key, @required this.otp}) : super(key: key);
  OtpRegistrationApp(this.email,this.mobile,this.otp,this.emailotp, {Key key}): super(key: key);

  @override
  OtpRoute createState()=>OtpRoute(email,mobile,otp,emailotp);
}

Future<VerifyUser1> verifyUser1(String email,String mobile) async {


  final http.Response response = await http.post(
      'http://www.skill-mine.com/demo/service/index.php/api/user/verifyUserMobile',
      /*  headers: <String, String>{
      'Content-Type': 'application/x-www-form-urlencoded',
    },*/
      body:
      {

        'mobile_num': mobile,
        'email' : email

      }
  );
  print(response.body);
  if (response.statusCode == 200) {
    return VerifyUser1.fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to verify user.');
  }
}

class VerifyUser1 {
  final bool status;
  final String message;
  final int otp1;
  final int emailotp;

  VerifyUser1({this.status, this.message,this.otp1,this.emailotp});

  factory VerifyUser1.fromJson(Map<String, dynamic> json) {
    return VerifyUser1(
      // status: json['status'],
        message: json['message'],
        otp1: json['st_otp'],
        emailotp: json['mail_otp']
    );
  }
}

Future<RegisterUser> registerUser(String email,String mobile,String password,String mobileotp,String mailotp) async {


  final http.Response response = await http.post(
      'http://www.skill-mine.com/demo/service/index.php/api/user/registerUser',
      /*  headers: <String, String>{
      'Content-Type': 'application/x-www-form-urlencoded',
    },*/
      body:
      {

        'mobile_num': mobile,
        'user_type' : '2',
        'password': password,
        'is_active': '1',
        'email' : email,
        'st_otp' : mobileotp,
        'mail_otp': mailotp




      }
  );
  print(response.statusCode);
  if (response.statusCode == 201) {
    return RegisterUser.fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to verify user.');
  }
}

class RegisterUser {
  final bool status;
  final String message;


  RegisterUser({this.status, this.message});

  factory RegisterUser.fromJson(Map<String, dynamic> json) {
    return RegisterUser(
      status: json['status'],
      message: json['message'],

    );
  }
}





class OtpRoute extends State<OtpRegistrationApp> {

  TextEditingController mobileotpController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  bool _isVisible = false;
  ProgressDialog pr1;
  int otp;
  int emailotpval;
  final String email;
  final String mobile;
  // int otp1;
  bool _isVerifyClicked = false;
  String _textString = 'Verify';
  bool isMobileOtpEnabled = true;
  bool isEnabled = true;
  bool pressVerify=true;
  bool isRegenerateClicked = true;
  bool viewVisible = false;
  Color color = new Color(Utility.hexToInt("FFF15922"));



  OtpRoute(this.email,this.mobile,this.otp,this.emailotpval, {Key key});



  void _changeButtonText() {
    // Using the callback State.setState() is the only way to get the build
    // method to rerun with the updated state value.
    setState(() {
      _textString = 'Verified';
      isMobileOtpEnabled = !isMobileOtpEnabled;
      pressVerify = !pressVerify;
      viewVisible = true;

    });
  }




  @override
  Widget build(BuildContext context) {

    pr1 = new ProgressDialog(context);
    pr1.style(
        message: 'Please Wait...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );

    return Scaffold(

        body: Padding(
            padding: EdgeInsets.all(10),


            child: ListView(
              children: <Widget>[







                Container(
                    alignment: Alignment.topLeft,
                    margin: EdgeInsets.all(15.0),
                    padding: EdgeInsets.only(top: 15.0),
                    child: Text(
                      'MOBILE OTP',
                      style: TextStyle(fontSize: 15),
                    )),

                Container(
                  padding: EdgeInsets.all(10),

                  child: TextField(
                    controller: mobileotpController..text = otp.toString(),
                    keyboardType: TextInputType.number,
                    enabled: isMobileOtpEnabled,




                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Mobile OTP',






                    ),



                  ),
                ),




                Container(

                  alignment: Alignment.center,

                  margin: EdgeInsets.all(10.0),
                  // padding: EdgeInsets.only(top: 10.0),



                  child:Text(
                    'An OTP has been sent to your mobile number',
                    style: TextStyle(fontSize: 14),


                  ),





                ),






                Container(
                    child: Row(


                      children: <Widget>[



                        FlatButton(


                          textColor: pressVerify ? new Color(Utility.hexToInt("FFF15922")) : Colors.grey,

                          // disabledColor: Color.fromARGB(0, 128,128,128),

                          padding: EdgeInsets.all(7),
                          //child: _isVerifyClicked ? Text("Verify",style: TextStyle(fontSize: 15),) : Text("Verified",style:TextStyle(fontSize: 15),),
                          child: Text(
                            _textString,
                            style: TextStyle(fontSize: 15),


                          ),
                          onPressed: () {

                            //signup screen
                            if(isRegenerateClicked) {
                              if (mobileotpController.text.length == 0) {
                                Fluttertoast.showToast(
                                    msg: "Please Enter OTP",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 1
                                );
                              }
                              else if (mobileotpController.text != otp.toString()) {
                                Fluttertoast.showToast(
                                    msg: "Wrong OTP",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 1
                                );
                              }
                              else {
                                if (isEnabled) {
                                  isEnabled = false;
                                  isRegenerateClicked = false;
                                  _changeButtonText();
                                }
                                //  isEnabled?null :() => _changeButtonText();


                              }
                            }
                            else
                            {
                              if (mobileotpController.text.length == 0) {
                                Fluttertoast.showToast(
                                    msg: "Please Enter OTP",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 1
                                );
                              }
                              else if (mobileotpController.text != otp.toString()) {
                                Fluttertoast.showToast(
                                    msg: "Wrong OTP",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 1
                                );
                              }
                              else {
                                if (isEnabled) {
                                  isEnabled = false;
                                  _changeButtonText();
                                }
                                //  isEnabled?null :() => _changeButtonText();


                              }

                            }

                          },

                        ),







                        FlatButton(
                          textColor: pressVerify ? new Color(Utility.hexToInt("FFF15922")) : Colors.grey,


                          padding: EdgeInsets.all(7),
                          child: Text(
                            'Re-generate OTP',
                            style: TextStyle(fontSize: 15),


                          ),
                          onPressed: () {


                            //signup screen
                            if(isRegenerateClicked) {
                              isRegenerateClicked = false;
                              pr1.show();
                              Future.delayed(Duration(seconds: 3)).then((value) {
                                pr1.hide().whenComplete(() {
                                  verifyUser1(email,
                                      mobile)
                                      .then((result) {
                                    print(result.otp1);
                                    otp = result.otp1;
                                    emailotpval = result.emailotp;

                                    if (result.message ==
                                        'Successfully sent OTP.') {
                                      mobileotpController.text =
                                          result.otp1.toString();
                                    }
                                  });
                                });
                              });
                            }

                          },
                        )
                      ],
                      mainAxisAlignment: MainAxisAlignment.center,
                    )),
                Visibility(
                    maintainSize: true,
                    maintainAnimation: true,
                    maintainState: true,
                    visible: viewVisible,
                    child:Container(
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                        child:TextField(
                          obscureText: false,
                          controller: newPasswordController,
                          keyboardType: TextInputType.number,

                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'New Password',
                          ),
                        )
                    )
                ),
                Visibility(
                    maintainSize: true,
                    maintainAnimation: true,
                    maintainState: true,
                    visible: viewVisible,
                    child:Container(
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                        child:TextField(
                          obscureText: false,
                          controller: confirmPasswordController,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Confirm Password',
                          ),
                        )
                    )
                ),

                Visibility(
                    maintainSize: true,
                    maintainAnimation: true,
                    maintainState: true,
                    visible: viewVisible,
                    child:Container(

                      margin: const EdgeInsets.only(top: 10.0,left: 20,right: 20),

                      child: Row(
                        children: <Widget>[




                          RaisedButton(

                            textColor: Colors.white,
                            color: new Color(Utility.hexToInt("FFF15922")),
                            child: Text('Submit'),
                            onPressed: () {

                              //   newPasswordController.text = '123456';
                              //   confirmPasswordController.text = '123456';


                              if ((newPasswordController.text.length > 5)&& (newPasswordController.text.length != 0) && (confirmPasswordController.text.length > 5)&& (confirmPasswordController.text.length != 0)) {

                                if(newPasswordController.text == confirmPasswordController.text)
                                {
                                  pr1.show();
                                  Future.delayed(Duration(seconds: 3)).then((value) {
                                    pr1.hide().whenComplete(() {

                                      // registerUser(String email,String mobile,String password,String mobileotp,String mailotp)
                                      registerUser(email,
                                          mobile,newPasswordController.text,otp.toString(),emailotpval.toString())
                                          .then((result) {
                                        print(result.message);


                                        if (result.message ==
                                            'Successfully Registered. Login to know more about Mxmart.in.') {

                                          Fluttertoast.showToast(
                                              msg: "You Have Successfully Registered.Please Login!!!",
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.CENTER,
                                              timeInSecForIosWeb: 1
                                          );
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => LoginApp()),
                                          );
                                        }
                                      });
                                    });
                                  });
                                }
                                else
                                {
                                  Fluttertoast.showToast(
                                      msg: "Not Matching OTP",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIosWeb: 1
                                  );
                                }

                              }
                              else
                              {
                                Fluttertoast.showToast(
                                    msg: "Enter Valid OTP",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 1
                                );
                              }



                            },

                          ),

                          Spacer(),
                          RaisedButton(


                            textColor: Colors.white,
                            color: new Color(Utility.hexToInt("FFF15922")),
                            child: Text('Cancel'),
                            onPressed: () {

                              Navigator.of(context).pop();


                            },

                          )
                        ],

                      ),

                    )
                )

              ],
            )


        )
    );
  }


}
