import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_app_mxmart/Forget_Password/forgetpassword.dart';
import 'package:flutter_app_mxmart/Home/home.dart';
import 'package:flutter_app_mxmart/SignUp/signup.dart';
import 'package:flutter_app_mxmart/Utility/utility.dart';
import 'dart:async';
import 'dart:convert';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:percent_indicator/percent_indicator.dart';

class LoginApp extends StatefulWidget {
  @override
  _State createState() => _State();

}

Future<Login> createLogin(String username,String password) async {


  final http.Response response = await http.post(
      'http://www.skill-mine.com/demo/service/index.php/api/user/login',
      /*  headers: <String, String>{
      'Content-Type': 'application/x-www-form-urlencoded',
    },*/
      body:
      {

        'password': password,
        'registration_Id': 'fre1Q-JFKVs:APA91bGsuCtXzd5TIhBPXy2tvEZMsjYZuvi89O_5n5IhYBkHwsgCdxaafWwmYoerF2fgQP33SjLTcfJL7XcDtNi-zQnl-yjDw_wEEQ5E4WBnNFFEqNtukwBCbi5oYdfr1d2zLOMHNI6q',
        'mobile_num' : username,
        'deviceType' : '1'

      }
  );
  print(response.body);
  if (response.statusCode == 200) {
    return Login.fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to create login.');
  }
}

class Login {
  final bool status;
  final String message;

  Login({this.status, this.message});

  factory Login.fromJson(Map<String, dynamic> json) {
    return Login(
      // status: json['status'],
      message: json['message'],
    );
  }
}


class _State extends State<LoginApp> {
  TextEditingController mobileController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  ProgressDialog pr;


  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    pr.style(
        message: 'Please Wait...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );

    return Scaffold(

        body: Padding(
            padding: EdgeInsets.all(10),

            child: ListView(
              children: <Widget>[


                Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(10),
                  child:
                  Image.asset(
                      'assets/images/tmlogo.png', height: 150, width: 150),
                  /*Text(
                      'TutorialKart',
                      style: TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.w500,
                          fontSize: 30),
                    )*/),
                /*Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(10),
                    child: Text(
                      'Sign in',
                      style: TextStyle(fontSize: 20),
                    )),*/
                Container(
                  padding: EdgeInsets.all(10),

                  child: TextField(
                    controller: mobileController,
                    keyboardType: TextInputType.number,


                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Mobile No.',


                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: TextField(
                    obscureText: true,
                    controller: passwordController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Password',
                    ),
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    //forgot password screen

                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ForgetPasswordApp()),
                    );
                  },
                  //textColor: new Color(hexToInt("000000")),
                  child: Text('Forgot Password?'),
                ),
                Container(
                    margin: const EdgeInsets.only(top: 50.0),
                    height: 50,
                    padding: EdgeInsets.fromLTRB(80, 5, 80, 5),
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: new Color(Utility.hexToInt("FFF15922")),
                      child: Text('Sign In'),
                      onPressed: () {
                        mobileController.text = '8050952462';
                        passwordController.text = 'saurabh21';


                        bool invalid = false;

                        if (Utility.validateMobile(mobileController.text) !=
                            null) {
                          invalid = true;


                          Fluttertoast.showToast(
                              msg: "Mobile No. Not Valid",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1
                          );
                        }
                        else if (passwordController.text.length < 6) {
                          invalid = true;
                          Fluttertoast.showToast(
                              msg: "Password Not Valid",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1
                          );
                        }
                        else if (invalid == false) {
                          print(mobileController.text);
                          print(passwordController.text);

                          pr.show();

                          Future.delayed(Duration(seconds: 3)).then((value) {
                            pr.hide().whenComplete(() {
                              createLogin(mobileController.text,
                                  passwordController.text)
                                  .then((result) {
                                print(
                                    'LOGIN RESPONSE MESSAGE ' + result.message);

                                if (result.message == 'Successfully Login.') {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => HomePageApp()),
                                  );
                                }
                              });
                            });
                          });


                          // setState(() {
                          // _futureLogin = createLogin(nameController.text,passwordController.text);


                          //  });

                        }
                      },

                    )


                ),


                Container(
                    child: Row(
                      children: <Widget>[

                        Padding(
                            padding: EdgeInsets.fromLTRB(30, 0, 0, 0),
                            child: Text('New User?',
                                style: TextStyle(fontSize: 15))),


                        FlatButton(
                          textColor: new Color(Utility.hexToInt("FFF15922")),
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: Text(
                            'Sign up',
                            style: TextStyle(fontSize: 15),


                          ),
                          onPressed: () {
                            //signup screen
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SignUpApp()),
                            );
                          },
                        )
                      ],
                      mainAxisAlignment: MainAxisAlignment.center,
                    ))
              ],
            )


        )
    );
  }
}




