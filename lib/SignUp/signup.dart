import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_app_mxmart/Forget_Password/forgetpassword.dart';
import 'package:flutter_app_mxmart/Otp_Registration/otpregistration.dart';
import 'package:flutter_app_mxmart/Utility/utility.dart';
import 'dart:async';
import 'dart:convert';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class SignUpApp extends StatefulWidget {
  @override

  SignupRoute createState()=>SignupRoute();
}

Future<VerifyUser> verifyUser(String email,String mobile) async {


  final http.Response response = await http.post(
      'http://www.skill-mine.com/demo/service/index.php/api/user/verifyUserMobile',
      /*  headers: <String, String>{
      'Content-Type': 'application/x-www-form-urlencoded',
    },*/
      body:
      {

        'mobile_num': mobile,
        'email' : email

      }
  );
  print(response.body);
  if (response.statusCode == 200) {
    return VerifyUser.fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to verify user.');
  }
}

class VerifyUser {
  final bool status;
  final String message;
  final int otp;
  final int emailotp;

  VerifyUser({this.status, this.message,this.otp,this.emailotp});

  factory VerifyUser.fromJson(Map<String, dynamic> json) {
    return VerifyUser(
      // status: json['status'],
        message: json['message'],
        otp: json['st_otp'],
        emailotp: json['mail_otp']

    );
  }
}



class SignupRoute extends State<SignUpApp> {

  TextEditingController emailController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  bool _isChecked = true;
  ProgressDialog pr1;

  @override
  Widget build(BuildContext context) {

    pr1 = new ProgressDialog(context);
    pr1.style(
        message: 'Please Wait...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );

    return Scaffold(

        body: Padding(
            padding: EdgeInsets.all(10),

            child: ListView(
              children: <Widget>[


                Container(
                  alignment: Alignment.center,
                  margin: const EdgeInsets.only(top: 5.0),
                  child:
                  Image.asset(
                      'assets/images/tmlogo.png', height: 150, width: 150),
                ),
                Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.only(bottom: 1.0),
                    child: Text(
                      'Create Account',
                      style: TextStyle(fontSize: 30),
                    )),

                Container(
                    alignment: Alignment.center,
                    //margin: EdgeInsets.all(),
                    padding: EdgeInsets.only(top: 10.0),
                    child: Text(
                      'Changing the way you buy metal',
                      style: TextStyle(fontSize: 15),
                    )),
                Container(
                    alignment: Alignment.center,
                    //margin: EdgeInsets.all(),
                    padding: EdgeInsets.only(top: 1.0),
                    child: Text(
                      'India first online refined metal store',
                      style: TextStyle(fontSize: 15),
                    )),
                Container(
                  padding: EdgeInsets.all(10),

                  child: TextField(
                    controller: emailController,


                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Email',





                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: TextField(
                    obscureText: false,
                    controller: mobileController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Mobile No.',
                    ),
                  ),
                ),
                Container(
                    margin: const EdgeInsets.only(top: 50.0),
                    height: 50,
                    padding: EdgeInsets.fromLTRB(80, 5, 80, 5),
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: new Color(Utility.hexToInt("FFF15922")),
                      child: Text('Create Account'),
                      onPressed: () {

                        //     emailController.text = 'saurabhmishra2971187@gmail.com';
                        //       mobileController.text = '9998079187';
                        print(emailController.text);
                        print(mobileController.text);

                        bool invalid = false;

                        if (Utility.validateEmail(emailController.text)!=null) {
                          invalid = true;
                          Fluttertoast.showToast(
                              msg: "Email Not Valid",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1
                          );
                        }

                        else if (Utility.validateMobile(mobileController.text) != null) {
                          invalid = true;


                          Fluttertoast.showToast(
                              msg: "Mobile No. Not Valid",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1
                          );
                        }
                        else if(_isChecked == false)
                        {
                          invalid = true;


                          Fluttertoast.showToast(
                              msg: "Please agree",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1
                          );
                        }

                        else if (invalid == false) {
                          print(emailController.text);
                          print(mobileController.text);

                          pr1.show();
                          Future.delayed(Duration(seconds: 3)).then((value) {
                            pr1.hide().whenComplete(() {
                              verifyUser(emailController.text,
                                  mobileController.text)
                                  .then((result) {
                                print(result.message);

                                if(result.message == 'Successfully sent OTP.')
                                {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => OtpRegistrationApp(emailController.text,mobileController.text,result.otp,result.emailotp)),
                                  );
                                }
                                else
                                {
                                  Fluttertoast.showToast(
                                      msg: "Already Registered",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIosWeb: 1
                                  );
                                }
                              });
                            });
                          });








                          // setState(() {
                          // _futureLogin = createLogin(nameController.text,passwordController.text);


                          //  });

                        }


                        // setState(() {
                        // _futureLogin = createLogin(nameController.text,passwordController.text);


                        //  });


                      },

                    )


                ),


                CheckboxListTile(
                  title: Text("I agree to the Mxmart Terms and Conditions"),
                  value: _isChecked,
                  onChanged: (newValue) {


                    setState((){
                      _isChecked = newValue;
                    });



                  },
                  controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                ),

                Container(
                    child: Row(
                      children: <Widget>[

                        Padding(
                            padding: EdgeInsets.fromLTRB(30, 0, 0, 0),
                            child: Text('Already have an account?',
                                style: TextStyle(fontSize: 15))),


                        FlatButton(

                          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: Text(
                            'Sign In',
                            style: TextStyle(fontSize: 15),


                          ),
                          onPressed: () {
                            //signup screen
                            Navigator.pop(context);

                          },
                        )
                      ],
                      mainAxisAlignment: MainAxisAlignment.center,
                    ))
              ],
            )


        )
    );
  }

}
