import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_app_mxmart/Login/login.dart';
import 'package:flutter_app_mxmart/Utility/utility.dart';
import 'package:flutter_app_mxmart/main.dart';
import 'dart:async';
import 'dart:convert';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:percent_indicator/percent_indicator.dart';



class ForgetPasswordApp extends StatefulWidget {

  @override
  ForgetPasswordRoute createState()=>ForgetPasswordRoute();
}

Future<UpdatePassword> updatePassword(String mobile,String password,String mobileotp) async {


  final http.Response response = await http.post(
      'http://www.skill-mine.com/demo/service/index.php/api/user/updatePassword',
      /*  headers: <String, String>{
      'Content-Type': 'application/x-www-form-urlencoded',
    },*/
      body:
      {

        'mobile_num': mobile,
        'password': password,
        'otp' : mobileotp,
        'type': '1'




      }
  );
  print(response.statusCode);
  if (response.statusCode == 200) {
    return UpdatePassword.fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to verify user.');
  }
}

class UpdatePassword {
  final bool status;
  final String message;


  UpdatePassword({this.status, this.message});

  factory UpdatePassword.fromJson(Map<String, dynamic> json) {
    return UpdatePassword(
      status: json['status'],
      message: json['message'],

    );
  }
}


Future<GetUserOTP> getUserOTP(String mobile) async {


  final http.Response response = await http.post(
      'http://www.skill-mine.com/demo/service/index.php/api/user/get_user_otp',
      /*  headers: <String, String>{
      'Content-Type': 'application/x-www-form-urlencoded',
    },*/
      body:
      {

        'mobile_num': mobile,



      }
  );
  print(response.statusCode);
  if (response.statusCode == 200) {
    return GetUserOTP.fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to verify user.');
  }
}

class GetUserOTP {
  final bool status;
  final String otp;


  GetUserOTP({this.status, this.otp});

  factory GetUserOTP.fromJson(Map<String, dynamic> json) {
    return GetUserOTP(
      status: json['status'],
      otp: json['otp'],

    );
  }
}


class ForgetPasswordRoute extends State<ForgetPasswordApp> {

  TextEditingController mobilenoController = TextEditingController();
  TextEditingController mobileotpController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  bool _isVisible = false;
  ProgressDialog pr1;
  int otp;
  String fpotp;
  int emailotpval;
  String email;
  String mobile;
  // int otp1;
  bool _isVerifyClicked = false;
  String _textString = 'Verify';
  bool isMobileOtpEnabled = true;
  bool isMobileNoEnabled = true;
  bool isEnabled = true;
  bool isMbSbmtEnabled = true;
  bool pressVerify=true;
  bool pressSubmit=true;
  bool isRegenerateClicked = true;
  bool viewVisible = false;
  bool viewSubmit = false;
  Color color = new Color(Utility.hexToInt("FFF15922"));



  //ForgetPasswordRoute(this.email,this.mobile,this.otp,this.emailotpval, {Key key});

  void _changeSubmitText() {
    // Using the callback State.setState() is the only way to get the build
    // method to rerun with the updated state value.
    setState(() {


      viewSubmit = true;
      isMobileNoEnabled = false;
      pressSubmit = false;

    });
  }



  void _changeButtonText() {
    // Using the callback State.setState() is the only way to get the build
    // method to rerun with the updated state value.
    setState(() {
      _textString = 'Verified';
      isMobileOtpEnabled = !isMobileOtpEnabled;
      pressVerify = !pressVerify;
      viewVisible = true;

    });
  }




  @override
  Widget build(BuildContext context) {

    pr1 = new ProgressDialog(context);
    pr1.style(
        message: 'Please Wait...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );

    return Scaffold(

        body: Padding(
            padding: EdgeInsets.all(10),


            child: ListView(
              children: <Widget>[

                Container(
                    alignment: Alignment.topCenter,
                    margin: EdgeInsets.all(15.0),
                    padding: EdgeInsets.only(top: 15.0),
                    child: Text(
                      'RESET PASSWORD',
                      style: TextStyle(fontSize: 15),
                    )),



                Container(
                  padding: EdgeInsets.all(10),

                  child: TextField(
                    controller: mobilenoController,
                    keyboardType: TextInputType.number,
                    enabled: isMobileNoEnabled,




                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Mobile No.',






                    ),



                  ),
                ),


                FlatButton(
                  textColor: pressSubmit ? new Color(Utility.hexToInt("FFF15922")) : Colors.grey,

                  padding:EdgeInsets.fromLTRB(0, 0, 0, 0),
                  child: Text(
                    'Submit',
                    style: TextStyle(fontSize: 15),



                  ),
                  onPressed: () {
                    //signup screen

                    mobilenoController.text = '8050952462';

                    if(isMbSbmtEnabled)
                    {

                      isMbSbmtEnabled = false;


                      bool invalid = false;

                      if (Utility.validateMobile(mobilenoController.text) != null) {
                        invalid = true;


                        Fluttertoast.showToast(
                            msg: "Mobile No. Not Valid",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIosWeb: 1
                        );
                      }

                      else if (invalid == false) {
                        print(mobilenoController.text);

                        pr1.show();
                        Future.delayed(Duration(seconds: 3)).then((value) {
                          pr1.hide().whenComplete(() {
                            getUserOTP(mobilenoController.text)
                                .then((result) {
                              print(result.otp);
                              fpotp = result.otp;


                              if (result.status ==
                                  true) {
                                _changeSubmitText();
                                mobileotpController.text =
                                    fpotp;
                              }
                            });
                          });
                        });
                      }

                    }

                  },
                ),

                Visibility(
                  maintainSize: true,
                  maintainAnimation: true,
                  maintainState: true,
                  visible: viewSubmit,
                  child: Container(
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.all(15.0),
                      padding: EdgeInsets.only(top: 15.0),
                      child: Text(
                        'MOBILE OTP',
                        style: TextStyle(fontSize: 15),
                      )),

                ),

                Visibility(
                  maintainSize: true,
                  maintainAnimation: true,
                  maintainState: true,
                  visible: viewSubmit,
                  child:Container(
                    padding: EdgeInsets.all(10),

                    child: TextField(
                      controller: mobileotpController,
                      keyboardType: TextInputType.number,
                      enabled: isMobileOtpEnabled,




                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Mobile OTP',






                      ),



                    ),
                  ),

                ),



                Visibility(
                  maintainSize: true,
                  maintainAnimation: true,
                  maintainState: true,
                  visible: viewSubmit,
                  child:  Container(

                    alignment: Alignment.center,

                    margin: EdgeInsets.all(10.0),
                    // padding: EdgeInsets.only(top: 10.0),



                    child:Text(
                      'An OTP has been sent to your mobile number',
                      style: TextStyle(fontSize: 14),


                    ),





                  ),


                ),



                Visibility(
                    maintainSize: true,
                    maintainAnimation: true,
                    maintainState: true,
                    visible: viewSubmit,
                    child: Container(
                        child: Row(


                          children: <Widget>[



                            FlatButton(


                              textColor: pressVerify ? new Color(Utility.hexToInt("FFF15922")) : Colors.grey,

                              // disabledColor: Color.fromARGB(0, 128,128,128),

                              padding: EdgeInsets.all(7),
                              //child: _isVerifyClicked ? Text("Verify",style: TextStyle(fontSize: 15),) : Text("Verified",style:TextStyle(fontSize: 15),),
                              child: Text(
                                _textString,
                                style: TextStyle(fontSize: 15),


                              ),
                              onPressed: () {

                                //signup screen
                                if(isRegenerateClicked) {
                                  if (mobileotpController.text.length == 0) {
                                    Fluttertoast.showToast(
                                        msg: "Please Enter OTP",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.CENTER,
                                        timeInSecForIosWeb: 1
                                    );
                                  }
                                  else if (mobileotpController.text != fpotp) {
                                    Fluttertoast.showToast(
                                        msg: "Wrong OTP",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.CENTER,
                                        timeInSecForIosWeb: 1
                                    );
                                  }
                                  else {
                                    if (isEnabled) {
                                      isEnabled = false;
                                      isRegenerateClicked = false;
                                      _changeButtonText();
                                    }
                                    //  isEnabled?null :() => _changeButtonText();


                                  }
                                }
                                else
                                {
                                  if (mobileotpController.text.length == 0) {
                                    Fluttertoast.showToast(
                                        msg: "Please Enter OTP",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.CENTER,
                                        timeInSecForIosWeb: 1
                                    );
                                  }
                                  else if (mobileotpController.text != fpotp) {
                                    Fluttertoast.showToast(
                                        msg: "Wrong OTP",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.CENTER,
                                        timeInSecForIosWeb: 1
                                    );
                                  }
                                  else {
                                    if (isEnabled) {
                                      isEnabled = false;
                                      _changeButtonText();
                                    }
                                    //  isEnabled?null :() => _changeButtonText();


                                  }

                                }

                              },

                            ),







                            FlatButton(
                              textColor: pressVerify ? new Color(Utility.hexToInt("FFF15922")) : Colors.grey,


                              padding: EdgeInsets.all(7),
                              child: Text(
                                'Re-generate OTP',
                                style: TextStyle(fontSize: 15),


                              ),
                              onPressed: () {


                                //signup screen
                                if(isRegenerateClicked) {
                                  isRegenerateClicked = false;
                                  print(mobilenoController.text);

                                  pr1.show();
                                  Future.delayed(Duration(seconds: 3)).then((value) {
                                    pr1.hide().whenComplete(() {
                                      getUserOTP(mobilenoController.text)
                                          .then((result) {
                                        print(result.otp);



                                        if (result.status ==
                                            true) {

                                          mobileotpController.text =
                                              result.otp;
                                          //_changeButtonText();
                                        }
                                      });
                                    });
                                  });
                                }

                              },
                            )
                          ],
                          mainAxisAlignment: MainAxisAlignment.center,
                        ))
                ),





                Visibility(
                    maintainSize: true,
                    maintainAnimation: true,
                    maintainState: true,
                    visible: viewVisible,
                    child:Container(
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                        child:TextField(
                          obscureText: false,
                          controller: newPasswordController,
                          keyboardType: TextInputType.number,

                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'New Password',
                          ),
                        )
                    )
                ),
                Visibility(
                    maintainSize: true,
                    maintainAnimation: true,
                    maintainState: true,
                    visible: viewVisible,
                    child:Container(
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                        child:TextField(
                          obscureText: false,
                          controller: confirmPasswordController,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Confirm Password',
                          ),
                        )
                    )
                ),

                Visibility(
                    maintainSize: true,
                    maintainAnimation: true,
                    maintainState: true,
                    visible: viewVisible,
                    child:Container(

                      margin: const EdgeInsets.only(top: 10.0,left: 20,right: 20),

                      child: Row(
                        children: <Widget>[




                          RaisedButton(

                            textColor: Colors.white,
                            color: new Color(Utility.hexToInt("FFF15922")),
                            child: Text('Submit'),
                            onPressed: () {

                              //  newPasswordController.text = 'saurabh21';
                              //   confirmPasswordController.text = 'saurabh21';


                              if ((newPasswordController.text.length > 5)&& (newPasswordController.text.length != 0) && (confirmPasswordController.text.length > 5)&& (confirmPasswordController.text.length != 0)) {

                                if(newPasswordController.text == confirmPasswordController.text)
                                {
                                  pr1.show();
                                  Future.delayed(Duration(seconds: 3)).then((value) {
                                    pr1.hide().whenComplete(() {

                                      // registerUser(String email,String mobile,String password,String mobileotp,String mailotp)
                                      updatePassword(
                                          mobilenoController.text,newPasswordController.text,mobileotpController.text)
                                          .then((result) {
                                        print(result.message);


                                        if (result.status ==
                                            true) {


                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => LoginApp()),
                                          );
                                        }
                                      });
                                    });
                                  });
                                }
                                else
                                {
                                  Fluttertoast.showToast(
                                      msg: "Not Matching OTP",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIosWeb: 1
                                  );
                                }

                              }
                              else
                              {
                                Fluttertoast.showToast(
                                    msg: "Enter Valid OTP",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 1
                                );
                              }



                            },

                          ),

                          Spacer(),
                          RaisedButton(


                            textColor: Colors.white,
                            color: new Color(Utility.hexToInt("FFF15922")),
                            child: Text('Cancel'),
                            onPressed: () {

                              Navigator.of(context).pop();


                            },

                          )
                        ],

                      ),

                    )
                )

              ],
            )


        )
    );
  }


}